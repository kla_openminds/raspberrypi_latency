#!/bin/bash

sudo systemctl enable ssh
sudo systemctl start ssh

sudo apt-get update -y
sudo apt-get upgrade -y

sudo apt-get install dnsmasq hostapd tcpdump -y

cat <<EOT | sudo tee -a /etc/dhcpcd.conf
interface wlan0
static ip_address=172.24.1.1/24
EOT

cat <<EOT | sudo tee -a /etc/network/interfaces
allow-hotplug wlan0
iface wlan0 inet manual
EOT

sudo echo "" > /etc/hostapd/hostapd.conf
cat <<EOT | sudo tee -a /etc/hostapd/hostapd.conf
interface=wlan0
driver=nl80211
ssid=Celltower#$RANDOM
hw_mode=g
channel=10
ieee80211n=1
wmm_enabled=1
ht_capab=[HT40][SHORT-GI-20][DSSS_CCK-40]
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_key_mgmt=WPA-PSK
wpa_passphrase=hugocell
rsn_pairwise=CCMP
EOT

sudo sed -ri 's/^#DAEMON_CONF=.*/DAEMON_CONF="\/etc\/hostapd\/hostapd\.conf"/' /etc/default/hostapd

cat <<EOT | sudo tee /etc/dnsmasq.conf
interface=wlan0      # Use interface wlan0  
listen-address=172.24.1.1 # Explicitly specify the address to listen on
#bind-interfaces      # Bind to the interface to make sure we aren't sending things elsewhere  
server=8.8.8.8       # Forward DNS requests to Google DNS  
domain-needed        # Don't forward short names  
bogus-priv           # Never forward addresses in the non-routed address spaces.  
dhcp-range=172.24.1.50,172.24.1.150,12h # Assign IP addresses between 172.24.1.50 and 172.24.1.150 with a 12 hour lease time
EOT


sudo sed -ri 's/^#net\.ipv4\.ip_forward.*/net.ipv4.ip_forward=1/' /etc/sysctl.conf

sudo sysctl -w net.ipv4.ip_forward=1
sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE  
sudo iptables -A FORWARD -i eth0 -o wlan0 -m state --state RELATED,ESTABLISHED -j ACCEPT  
sudo iptables -A FORWARD -i wlan0 -o eth0 -j ACCEPT

sudo sh -c "iptables-save > /etc/iptables.ipv4.nat"


cat <<EOT | sudo tee /lib/dhcpcd/dhcpcd-hooks/70-ipv4-nat
iptables-restore < /etc/iptables.ipv4.nat
EOT


sudo systemctl enable hostapd
sudo systemctl enable dnsmasq
sudo service hostapd start  
sudo service dnsmasq start
sudo reboot


